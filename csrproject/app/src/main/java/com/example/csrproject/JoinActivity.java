package com.example.csrproject;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class JoinActivity extends AppCompatActivity {

    Button validateButton, phonecheckButton;
    ImageView btnBack, btnClose;
    EditText idText, phonenumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);

        validateButton = (Button) findViewById(R.id.validateButton);
        phonecheckButton = (Button) findViewById(R.id.phonecheckButton);
        btnBack = (ImageView) findViewById(R.id.btnBack);
        btnClose = (ImageView) findViewById(R.id.btnClose);

        idText = (EditText) findViewById(R.id.idText);
        phonenumber = (EditText) findViewById(R.id.phonenumber);

        validateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (idText.getText().toString().equals("admin") || idText.getText().toString().equals("user")) {
                    Toast.makeText(getApplicationContext(), "아이디 중복입니다.", Toast.LENGTH_LONG).show();
                }

                else {
                    Toast.makeText(getApplicationContext(), "사용 가능한 아이디입니다.", Toast.LENGTH_LONG).show();
                }
            }
        });

        phonecheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "인증화면으로 넘어갑니다.", Toast.LENGTH_LONG).show();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
