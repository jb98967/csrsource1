package com.example.csrproject;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class Move extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_move);

        Button movepass = (Button)findViewById(R.id.movepass);
        movepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent NoPage = new Intent(Move.this, PassWord.class);
                startActivity(NoPage);
            }
        });
    }
}