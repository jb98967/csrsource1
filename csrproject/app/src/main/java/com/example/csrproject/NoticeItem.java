package com.example.csrproject;

import android.graphics.drawable.Drawable;

public class NoticeItem {

    private Drawable noticeImage;
    private String noticeTitle;

    public void setImage(Drawable image) {
        this.noticeImage = image;
    }

    public void setNoticeTitle(String title) {
        this.noticeTitle = title;
    }

    public Drawable getImage() {
        return noticeImage;
    }

    public String getNoticeTitle() {
        return noticeTitle;
    }

}