package com.example.csrproject;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;

public class BeginActivity extends AppCompatActivity {

    EditText idText, passwordText;
    Button joinButton, loginButton;
    CheckBox autologin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_begin);

        idText = (EditText) findViewById(R.id.idText);
        passwordText = (EditText) findViewById(R.id.passwordText);
        joinButton = (Button) findViewById(R.id.BtnRegister);
        loginButton = (Button) findViewById(R.id.loginButton);
        autologin = (CheckBox) findViewById(R.id.autologin);


        // 자동로그인 체크된 경우
        if (autologin.isChecked() == true) {

        }

        joinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BeginActivity.this, JoinActivity.class);
                startActivity(intent);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String id = idText.getText().toString();
                String pw = passwordText.getText().toString();

                // admin인 경우 대기화면 대체 (원래 관리자 화면이어야함)
                if (id.equals("admin") && pw.equals("admin")) {
                    Intent intent = new Intent(BeginActivity.this, MainActivity.class);
                    startActivity(intent);
                }

                // user인 경우 회원가입화면 대체 (원래 유저 화면이어야함)
                else if (id.equals("user") && pw.equals("user")) {
                    Intent intent = new Intent(BeginActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
}
