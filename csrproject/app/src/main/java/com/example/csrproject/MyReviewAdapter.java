package com.example.csrproject;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import androidx.appcompat.app.AppCompatActivity;

public class MyReviewAdapter extends BaseAdapter {
    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList
    private ArrayList<MyReviewItem> myReviewItemList = new ArrayList<MyReviewItem>() ;

    // ListViewAdapter의 생성자
    public MyReviewAdapter() {

    }

    // Adapter에 사용되는 데이터의 개수를 리턴. : 필수 구현
    @Override
    public int getCount() {
        return myReviewItemList.size() ;
    }

    // position에 위치한 데이터를 화면에 출력하는데 사용될 View를 리턴. : 필수 구현
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int pos = position;
        final Context context = parent.getContext();

        // "listview_item" Layout을 inflate하여 convertView 참조 획득.
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_review_item, parent, false);
        }

        // 화면에 표시될 View(Layout이 inflate된)으로부터 위젯에 대한 참조 획득
        ImageView reviewImage = (ImageView) convertView.findViewById(R.id.reviewImage);
        RatingBar ratingBar  = (RatingBar) convertView.findViewById(R.id.reviewRating);
        TextView reviewContents = (TextView) convertView.findViewById(R.id.reviewContents);

        // Data Set(listViewItemList)에서 position에 위치한 데이터 참조 획득
        MyReviewItem myReviewItem = myReviewItemList.get(position);

        // 아이템 내 각 위젯에 데이터 반영
        reviewImage.setImageDrawable(myReviewItem.getImage());
        ratingBar.setRating(myReviewItem.getRatingStar());
        reviewContents.setText(myReviewItem.getContents());

        return convertView;
    }

    // 지정한 위치(position)에 있는 데이터와 관계된 아이템(row)의 ID를 리턴. : 필수 구현
    @Override
    public long getItemId(int position) {
        return position ;
    }

    // 지정한 위치(position)에 있는 데이터 리턴 : 필수 구현
    @Override
    public Object getItem(int position) {
        return myReviewItemList.get(position) ;
    }

    // 아이템 데이터 추가를 위한 함수. 개발자가 원하는대로 작성 가능.
    public void addItem(Drawable icon, float ratingstar, String contents) {
        MyReviewItem item = new MyReviewItem();

        item.setImage(icon);
        item.setRatingStar(ratingstar);
        item.setContents(contents);

        myReviewItemList.add(item);
    }
}
