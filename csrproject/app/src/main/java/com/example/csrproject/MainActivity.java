package com.example.csrproject;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TabHost;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

public class MainActivity extends TabActivity {

    Button btnCancel, btnNext, btnPerson, btnVersion, btnNotification, btnNotice;
    ImageView imgSearch;
    ListView reviewListView;
    MyReviewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnNext = (Button) findViewById(R.id.btnNext);
        btnCancel = (Button) findViewById(R.id.btnCancel);

        btnPerson = findViewById(R.id.btnPerson);
        btnVersion = findViewById(R.id.btnVersion);
        btnNotification = findViewById(R.id.btnNotification);
        btnNotice = findViewById(R.id.btnNotice);

        imgSearch = (ImageView) findViewById(R.id.imgSearch);
        adapter = new MyReviewAdapter();

        reviewListView = (ListView) findViewById(R.id.searchListView);
        reviewListView.setAdapter(adapter);

        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.coin_singingroom_sample), (float) 1, "좋아요1");
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.coin_singingroom_sample), (float) 2, "좋아요2");
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.coin_singingroom_sample), (float) 3, "좋아요3");
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.coin_singingroom_sample), (float) 4, "좋아요4");
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.coin_singingroom_sample), (float) 5, "좋아요5");

        TabHost tabHost = getTabHost();

        TabHost.TabSpec tabSpecTab1 = tabHost.newTabSpec("TAB1").setIndicator("홈");
        tabSpecTab1.setContent(R.id.tab1);
        tabHost.addTab(tabSpecTab1);

        TabHost.TabSpec tabSpecTab2 = tabHost.newTabSpec("TAB2").setIndicator("검색");
        tabSpecTab2.setContent(R.id.tab2);
        tabHost.addTab(tabSpecTab2);

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        TabHost.TabSpec tabSpecTab3 = tabHost.newTabSpec("TAB3").setIndicator("예약");
        tabSpecTab3.setContent(R.id.tab3);
        tabHost.addTab(tabSpecTab3);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, MainActivity.class);
                finish();
                startActivity(i);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, RoomActivity.class);
                finish();
                startActivity(i);
            }
        });

        TabHost.TabSpec tabSpecTab4 = tabHost.newTabSpec("TAB4").setIndicator("더보기");
        tabSpecTab4.setContent(R.id.tab4);
        tabHost.addTab(tabSpecTab4);

        btnPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Move.class);
                startActivity(i);
            }
        });


        btnVersion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Move.class);
                startActivity(i);
            }
        });

        btnNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Move.class);
                startActivity(i);
            }
        });

        btnNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, NoticeActivity.class);
                startActivity(i);
            }
        });


        tabHost.setCurrentTab(0);
    }
}
