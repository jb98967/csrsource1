package com.example.csrproject;

import android.graphics.drawable.Drawable;

public class MyReviewItem {

    private Drawable reviewImage;
    private float ratingStar;
    private String reviewContents;

    public void setImage(Drawable image) {
        this.reviewImage = image;
    }

    public void setRatingStar(float ratingStar) {
        this.ratingStar = ratingStar;
    }

    public void setContents(String contents) {
        this.reviewContents = contents;
    }

    public Drawable getImage() {
        return reviewImage;
    }

    public float getRatingStar() {
        return ratingStar;
    }

    public String getContents() {
        return reviewContents;
    }

}