package com.example.csrproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import android.os.Bundle;
import android.widget.ListView;

public class ReviewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        ListView reviewListView;
        MyReviewAdapter adapter;

        adapter = new MyReviewAdapter();

        reviewListView = (ListView)findViewById(R.id.reviewListView);
        reviewListView.setAdapter(adapter);

        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.coin_singingroom_sample), (float)1, "좋아요1");
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.coin_singingroom_sample), (float)2, "좋아요2");
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.coin_singingroom_sample), (float)3, "좋아요3");
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.coin_singingroom_sample), (float)4, "좋아요4");
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.coin_singingroom_sample), (float)5, "좋아요5");
    }
}