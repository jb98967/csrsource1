package com.example.csrproject;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;

import androidx.appcompat.app.AppCompatActivity;

public class MyReservation extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myreservation);
        TableLayout myReservationTable = (TableLayout) findViewById(R.id.myReservationTable);
        TableRow tr = new TableRow(this);
        Button btnAdd = (Button) findViewById(R.id.btnAdd);
        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT);

        tr.setLayoutParams(lp);
        btnAdd.setLayoutParams(lp);
        btnAdd.setText("I NEED HELP ! SOS");

        tr.addView(btnAdd);
        myReservationTable.addView(tr, lp);
    }
}
