package com.example.csrproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class CreditCompleteActivity extends AppCompatActivity {

    Button btnOk, btnCancel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_complete);

        btnOk = (Button)findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplication(),"버튼이눌렸습니다.",Toast.LENGTH_LONG).show();
                Intent OkPage = new Intent(CreditCompleteActivity.this, MainActivity.class);
                finish();
                startActivity(OkPage);
            }
        });

        btnCancel = (Button)findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent NoPage = new Intent(CreditCompleteActivity.this, MainActivity.class);
                finish();
                startActivity(NoPage);
            }
        });
    }
}
