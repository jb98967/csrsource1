package com.example.csrproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.widget.ListView;

public class NoticeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);

        ListView noticeListView;
        NoticeAdapter adapter;

        adapter = new NoticeAdapter();

        noticeListView = (ListView) findViewById(R.id.noticeListView);
        noticeListView.setAdapter(adapter);

        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.question_mark_icon), "공지사항 1");
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.question_mark_icon), "공지사항 2");
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.question_mark_icon), "공지사항 3");
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.question_mark_icon), "공지사항 4");
        adapter.addItem(ContextCompat.getDrawable(this, R.drawable.question_mark_icon), "공지사항 5");

    }
}
